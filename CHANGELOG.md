# Changelog

## v0.1.1

- Add autocommands for more supported Code Suggestions languages (!45)

## v0.1.0

- Document Code Suggestions completion and add `fix_newlines` option (!29)
- Add Omni completion support through the Neovim LSP client (!22)
- Setup Lua module and plugin structure (!1)
